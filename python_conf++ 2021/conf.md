# Трансляции конференции

Ссылка на все записи: https://www.youtube.com/playlist?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7
Если кто не знал, можно увеличить скорость воспроизведения до 1.5 - 1.75, слушать не мешает, но время экономит знатно

- ## Что надо будет посмотреть:

- Cookiecutter https://github.com/cookiecutter/cookiecutter, утилита для создания проектов из шаблонов, есть библиотека для django

- Deal https://github.com/life4/deal, утилита для контрактного программирования: помимо типизации функции можно указать и параметры контракта - ограничения на входящие и исходящие данные, а также указать какие функции являются чистыми (Pure) и получить экспепшен, если контракт не выполняется

- Hypothesis https://hypothesis.readthedocs.io/en/latest/, утилита, которая генерирует юнит-тесты для функций, у которых указаны контракты. 

- ugettext_lazy https://stackoverflow.com/questions/4160770/when-should-i-use-ugettext-lazy, нужно использовать когда используем мультиязычность

# DevOPS

- ## Шаблонизация микросервисов 
https://youtu.be/uFpTJ2I6lZc?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=13528

Ссылка на сам шаблон: https://github.com/Bahus/cookiecutter-pyservice


# Сеть, бэкенд и web-разработка

- ## Зачем нужен Dgango в 2021
https://youtu.be/uFpTJ2I6lZc?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=3120

- ## Документация в TDD (Test driven development)
https://youtu.be/NlP1Gsfv-rc?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=10219

- ## Про логирование
https://youtu.be/CnczoPpkuBs?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=3153

- ## Почему переехали на FastAPI c Flask
https://youtu.be/5PJ6FYOwfUI?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=303

- ## Двусторонний websocket роутинг
https://youtu.be/5PJ6FYOwfUI?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=11574


# AI/ML

- ## Зачем нам всем нужен JupiterHub 
https://youtu.be/uFpTJ2I6lZc?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=18077

- ## Авторматизируем саморефлексию ботами и дашбордами
https://youtu.be/uFpTJ2I6lZc?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=23411

- ## Python для data-science
https://youtu.be/NlP1Gsfv-rc?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=2050

- ## Расширения Jupiter
https://youtu.be/NlP1Gsfv-rc?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=6314


# Всякое про python

- ## Как работать с legacy
https://youtu.be/uFpTJ2I6lZc?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=7760

- ## Почему не нужен асинхронный ORM
https://youtu.be/NlP1Gsfv-rc?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=15976

- ## Зачем нужны subinterpreters
https://youtu.be/NlP1Gsfv-rc?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=22421

- ## RPA как основа автоматизации на python
https://youtu.be/CnczoPpkuBs?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=7419

- ## Тесты, которые мы заслужили
https://youtu.be/5PJ6FYOwfUI?list=PL47DhcO78AnH1YiPFY4EA8UO9HP18nfy7&t=16098

