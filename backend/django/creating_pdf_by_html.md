### Для создания PDF документа из HTML необходимо выполнить следующие действия:

1. установить пакет: ***pip install xhtml2pdf***
2. создать callback-функцию ***fetch_pdf_resources***
3. создать функцию ***render_to_pdf***
4. создать представление (View) на основе **CBV** или **function**
5. указать созданное представление (View) в **urls.py**

****
### Callback-функция **fetch_pdf_resources**

    import os
    from typing import Any, Optional
    from xhtml2pdf import pisa
    from django.http import HttpResponse
    from django.template.loader import get_template
    from io import BytesIO
    from your_project_name import settings

    def fetch_pdf_resources(uri: str, rel: Any):
        """Function for fetching static resources"""
        path = None
        if uri.find(settings.MEDIA_URL) != -1:
            path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ''))
        elif uri.find(settings.STATIC_URL) != -1:
            path = os.path.join(settings.STATIC_ROOT, uri.replace(settings.STATIC_URL, ''))
        return path

### Функция **render_to_pdf**
   
    def render_to_pdf(template_src, context_dict={}) -> Optional[HttpResponse]:
        """Function for rendering PDF document"""
        FONT_PATH = STATIC_URL + 'path/to/your/fonts'
        template = get_template(template_src)
        html = template.render(context_dict)
        result = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode('UTF-8')), result, path=FONT_PATH, encoding='UTF-8', link_callback=fetch_pdf_resources)
        if not pdf.err:
            return HttpResponse(result.getvalue(), content_type='application/pdf')
        return None

### Представление **View** на основе CBV или function
    # В данном примере используется View на основе function
    # Существующие модели замените на свои
    # параметры `pk`, `m`, `y` необходимо заменить на нужные вам, или убрать вообще, если в них нет необходимости

    def print_report_for_gis(request, pk: int, m: int, y: int):
        employee = Employee.objects.get(pk=pk)
        date_report = datetime.date(day=1, month=m, year=y)
        tasks = Task.objects.filter(date_beginning__month=m).filter(date_beginning__year=y).filter(
            sotry=pk).order_by('date_beginning')
        count = Task.objects.filter(date_beginning__month=m).filter(date_beginning__year=y).filter(
            sotry=pk).count

        # Формируем словарь `context`. К сущностям в контексте будет доступ из HTML шаблона через шаблонизатор Jinja
        context = {'tasks': tasks, 'employee': employee, 'count': count, 'date_report': date_report}
        
        # Вызываем функцию `render_to_pdf`
        # Передаем путь к шаблону и контекст
        pdf = render_to_pdf('rootApp/gis/analytics/report_print.html', context)
        
        # Рендерим страничку
        return HttpResponse(pdf, content_type='application/pdf')


### Финальные действия:
Далее необходимо указать созданное Представление в urls.py

### Тесты:
В HTML-страничке, предшествуещей PDF-файлу необходимо указать код в таком формате:
        
    <a href="{% url 'print_report_for_gis_url' pk=employee.pk m=month y=year %}" type="button" class="btn btn-dark btn-sm mb-2">
        Печать в PDF
    </a>
    
***print_report_for_gis_url*** - содержимое параметра **name** при добавлении View в urlpatterns:
    
    path('gis/report/employee/print/<int:pk>?<int:m>?<int:y>', print_report_for_gis, name='print_report_for_gis_url'),

**pk**, **m**, **y** - параметры, указанные во View (Указать свои параметры, если такие имеются)

Ссылку/кнопку можно стилизовать как угодно. В ней смысл один - перейти по ней, и сгенерировать PDF
