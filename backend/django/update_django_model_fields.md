На проектах часто сталкиваюсь с тем, что нужно обновить какое то поле модели, а постоянно писать

obj.field = 'something'
obj.save()

не хочется

написал метод, который позволяет обновить одно или несколько полей
    def update_fields(self, save: bool = False, **kwargs):
        """
        :param save: if True, obj will be saved
        :param kwargs: dict(field_name=value)
        """
        for key, value in kwargs.items():
            if hasattr(self, key):
                update_field = getattr(self, key)
                if update_field != value:
                    setattr(self, key, value)
        self.save() if save else None
        
и теперь можно спокойно обновлять поля
obj.update_fields(foo='bar', foo_1='bar_1', save=True)
