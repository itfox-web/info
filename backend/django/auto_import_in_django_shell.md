# Эта фича поможет тем, кто часто пользуется командой manage.py shell.
Мне стало надоедать постоянно импортировать модели из своих приложений, и я решил написать небольшой скриптик, который будет автоматически импортировать все модели, сериализаторы (и в принципе, что укажете, то и импортнет) из всех моих приложений.
Да, есть модуль **django-extensions** который много чего другого и это в том числе, но хотелось написать самому.
Для начала, выбираем приложение, создаем в корне папки приложения папки management/commands
Создаем ** __init__.py**, **script.py**, и файл команды - у меня это **supershell.py**

```sh
В supershell.py пишем следующий код
from os.path import join, dirname
from django.core.management.commands.shell import Command as ShellCommand
from dotenv import set_key, load_dotenv # я пользуюсь пакетом dotenv

load_dotenv() # подгружаем .env файл


class Command(ShellCommand):
    help = 'Run shell with importing all projects models'

    def handle(self, *args, **kwargs):
        dotenv_path = join(dirname(__file__), '.env')

        # Указываем путь к script.py
        script_path = 'auth_app//management//commands//script.py'

        set_key(dotenv_path, 'PYTHONSTARTUP', script_path)
        # Переменная среды PYTHONSTARTUP отвечает за то, какой файл должен будет выполниться,
        # когда интерпретатор запустится

        # Когда выполнится эта команда, в папке /commands создастся .env файл
        # в котором будет запись PYTHONSTARTUP="auth_app//management//commands//script.py"
        # не переживайте, он никак не пересекается с главным .env файлом

        super(Command, self).handle(*args, **kwargs)
```

```sh
В scripts.py пишем следующий код:
from django.apps import apps

# Получаем словарь типа {'name_app': {'name_model': ModelClass}}
all_models = apps.all_models

# Выбираем приложения, в которых есть модели
applications = [app for app in all_models if all_models.get(app) != {}]

# Поочередно импортируем модули, которые вам нужны, и игнорируем приложения,
# из которых эти модули импортировать не удалось
for app in applications:
    try:
        exec(f'from {app}.models import *')
        exec(f'from {app}.serializers import *')
    except ModuleNotFoundError:
        continue
```

При первом запуске **manage.py supershell**, создается создается **.env** файл. Консоль запускается, но модели еще не импортируются, так как файл **.env** в папке /commands еще не подхватился.
Последующие запуски **manage.py supershell** уже будут с предварительно импортированными моделями и иными сущностями, которые указаны в **scripts.py**.