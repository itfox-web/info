#!/bin/bash
apt update
apt install -y vim docker.io docker-compose zsh git curl
# Install OH MY ZSH 
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
