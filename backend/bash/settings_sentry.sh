#!/usr/bin/bash

# Устанавливаем политику вытеснения и ограничение на кол-во используемой пямяти для редис
export SENTRY_REDIS=sentry-self-hosted-redis-1 
docker exec -it $SENTRY_REDIS redis-cli CONFIG SET maxmemory-policy volatile-ttl
docker exec -it $SENTRY_REDIS redis-cli CONFIG SET maxmemory 8G 

#NOTE: these configs will be removed when the container restarts, so the long term solution is to mount updated redis.conf to the container.

 export KAFKA=sentry-self-hosted-kafka-1
# set retention for 1sec and wait for cleanup
docker exec $KAFKA -it kafka-configs --zookeeper zookeeper:2181 --entity-type topics --alter --entity-name ingest-events --add-config retention.ms=1000
docker exec $KAFKA kafka-configs --zookeeper zookeeper:2181 --entity-type topics --alter --entity-name events --add-config retention.ms=1000
# revert retention back
docker exec $KAFKA kafka-configs --zookeeper zookeeper:2181 --entity-type topics --alter --entity-name ingest-events --delete-config retention.ms
docker exec $KAFKA kafka-configs --zookeeper zookeeper:2181 --entity-type topics --alter --entity-name events --delete-config retention.ms


