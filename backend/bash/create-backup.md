## Скрипт для создания backup и сохранение его в s3 хранилище

- Создаем файл backup.sh
- Скачиваем библиотек awscli

```bash
pip install awscli
```
- Добавляем ключи в awscli

```bash
aws configure
```
- Добавляем переменные в файл .env
- ENDPOINT_URL - если провайдер aws можно не указывать
- BACKUP_CONTAINER - название s3 хранилища

- Копируем и вставляем указанный ниже код в файл

```bash

#! /bin/bash

# Переходим в деректорию где находится скрипт
cd /app/
# Подтягиваем переменные 
source .env
# Указываем путь к библиотекам
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
# Добавляем переменную с названием файла
FILE=backups/dump_`date +%d-%m-%Y"_"%H-%M-%S`.sql.gz
# Получаем контейнер с базой данных
CONTAINER=$(docker container ls | grep postgres | cut -d " " -f1)
# Создаем backup
docker exec -t $CONTAINER pg_dump --username $POSTGRES_DB_USER $POSTGRES_DB_PASSWORD | gzip >  $FILE

# Сохраняем в backup в s3 хранилище 
aws --endpoint-url=$ENDPOINT_URL s3 cp $FILE s3://$BACKUP_CONTAINER/Dump/$FILE
# Удаляем backup
rm $FILE

```