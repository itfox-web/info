#!/bin/bash

red=$(tput setaf 1)
green=$(tput setaf 2)

getShellName() {
	read -pr "${green}[+] What shell are you using (bash, fish, zsh): ${reset}" SHELL_NAME
	if [[ $SHELL_NAME != 'bash' && $SHELL_NAME != 'zsh' && $SHELL_NAME != 'fish' ]]; then
		echo "${red}Unknown shell${reset}"
		exit
	fi
}

setEnvironmentVariables() {
	read -pr "${green}[+] Enter project name: ${reset}" KUBE_NAME
	if [[ -z $KUBE_NAME ]]; then
		echo "${green}[+] Project name missing: ${reset}"
		exit
	fi

	read -pr "${green}[+] Enter node name: ${reset}" KUBE_NODE
	if [[ -z $KUBE_NODE ]]; then
		if [[ $SHELL_NAME == 'bash' || $SHELL_NAME == 'zsh' ]]; then
			KUBE_NODE=main-node-khxth
		else
			set KUBE_NODE main-node-khxth
		fi
	fi

	read -pr "${green}[+] Enter user email: ${reset}" KUBE_USER_EMAIL
	# read -p "${green}[+] Enter node role: ${reset}" KUBE_ROLE;
	read -pr "${green}[+] Enter domain name: ${reset}" DEV_DOMAIN

	KUBE_NS=$KUBE_NAME-ns
	KUBE_IMAGE_PULL=$KUBE_NS-image-pull
	KUBE_SERVICE_NAME=$KUBE_NAME-service
	KUBE_SECRET=$KUBE_NAME-secret-ts
	KUBE_LABEL="$KUBE_NAME=app"
	KUBE_LABEL_DEPLOYMENT="$KUBE_NAME: app"

	kubectl create namespace $KUBE_NS
	kubectl label node $KUBE_NODE $KUBE_LABEL

createImagePullSecret() {
	echo -e 'Зарегистрируйте deploy token в GitLab\n'

	read -p "${green}[+] Enter gitlab token name: ${reset}" TOKEN_NAME
	read -p "${green}[+] Enter gitlab token password: ${reset}" TOKEN_PASSWORD

	kubectl create secret docker-registry $KUBE_IMAGE_PULL --docker-server registry.gitlab.com --docker-email $KUBE_USER_EMAIL --docker-username $TOKEN_NAME --docker-password $TOKEN_PASSWORD --namespace $KUBE_NS
	echo "${green}[+] Image Pull Successful created${reset}"
}

createImagePullPolicy() {
	echo 'Создать сервисную учетную запись'
	kubectl create sa deploy -n $KUBE_NS

	echo "Предоставить ей права на создание объектов внутри namespace ${KUBE_NS}"
	kubectl create rolebinding deploy \
		-n $KUBE_NS \
		--clusterrole edit \
		--serviceaccount $KUBE_NS:deploy

	echo -e 'Получаем Token для настройки CI/CD\n'

	TOKEN=$(kubectl get secret -n $KUBE_NS \
		$(kubectl get sa -n $KUBE_NS deploy \
			-o jsonpath='{.secrets[].name}') \
		-o jsonpath='{.data.token}')
}

createWebDeployment() {
	#Create web deployment
	if [[ -d kubernetes ]]; then
		echo "${green}[+] All files will be created in the kubernetes folder${reset}"
	else
		mkdir kubernetes
	fi

	if [[ -d kubernetes/web-deployment.yaml ]]; then
		echo "${red}[-] web_deployment.yaml already exist${reset}"
	else
		echo "${green}[+] Create web_deployment.yaml${reset}"
		touch kubernetes/web-deployment.yaml

		echo '
apiVersion: apps/v1
# Тип размещения Deployment
kind: Deployment
metadata:
  # Представление пода 
  name: <KUBE_NAME>
  # Пространство имен проекта
  namespace: <K8S_NAMESPACE>
  labels:
    # Метка деплоймента
    app: <KUBE_NAME>
spec:
  # Колиkчество реплик - 1
  replicas: 1
  selector:
    matchLabels:
      # Метка пода
      app: <KUBE_NAME>
  template:
    metadata:
      labels:
        # Метка контейнера
        app: <KUBE_NAME>
    spec:
      imagePullSecrets:
        # Секрет для получения docker изображения из приватного хранилища (GitLab)
        # Ниже будет инструкция по его созданию
        - name: <KUBE_IMAGE_PULL>
      containers:
        # Образ для контейнера
        - image: <IMAGE> 
        # Название контейнера
          name: web
        # Команда запуска проекта
          command: ["/bin/sh"]
          args: ["-c", "python manage.py migrate --no-input 
            && python manage.py collectstatic --no-input 
            && python manage.py createcachetable
            && gunicorn conf.asgi:application -k uvicorn.workers.UvicornWorker --workers=1 --timeout 30 --max-requests=10000 --bind 0.0.0.0:8000"]
          envFrom:
            - configMapRef:
                # Название configmap от куда будут подтягиваться переменные окружения
                name: <K8S_ENV>
          resources:
            # Количество запрашиваемых ресурсов для контейнера при старте
            requests:
              cpu: 100m
              memory: 500Mi
            # Лимит ресурсов для контейнера
            limits:
              cpu: 300m
              memory: 1000Mi 
          # Указываем палитику для загрузки образа контейнера
          # Данная политика указывает что если образ уже загружен на ноду то не загружать его снова                          
          imagePullPolicy: IfNotPresent
          # Подставляется при CI/CD Gitlab
          ports:
            # Порт на котором будет запущено приложение
            - containerPort: 8000
          volumeMounts:
            # Название контейнера шарится внутри деплоймента в любой контейнер
            # Если нужно шарить глобально в ноде нужно создавать Persictem Volume
            - name: static
              mountPath: /app/static/
              
        # Если нужен Celery
        # Контейнер celery-beat
        - image: <IMAGE> 
          name: celery-beat
          command: [celery, -A, conf, beat, -l, DEBUG]
          envFrom:
            - configMapRef:
                name: <K8S_ENV>
          resources:
            requests:
              cpu: 2m
              memory: 116Mi
            limits:
              cpu: 100m
              memory: 200Mi 
          imagePullPolicy: IfNotPresent
          
        # Контейнер celery-worker
        - image: <IMAGE>
          name: celery-worker
          command: [celery, -A, conf, worker, -l, INFO]
          envFrom:
            - configMapRef:
                name: <K8S_ENV>
          resources:
            requests:
              cpu: 4m
              memory: 209Mi
            limits:
              cpu: 10m
              memory: 400Mi 
          imagePullPolicy: IfNotPresent
          
        # Контейнер для nginx
        - image: nginx:1.19.0-alpine
          name: nginx
          ports:
          - containerPort: 80
          resources:
            requests:
              cpu: 2m
              memory: 30Mi
            limits:
              cpu: 100m
              memory: 200Mi 
          imagePullPolicy: IfNotPresent

          volumeMounts:
            # Прокидываем конфиг в nginx
            # Название configmap для nginx
            - name: nginx-config
              mountPath: /etc/nginx/conf.d/
            # Название configmap для web
            - name: static
              mountPath: /app/static/
        
      volumes:
        - name: static
        - name: nginx-config
          configMap:
            name: nginx-config
      # Нод селектор при планировки деплоймента будут искаться ноды с этой меткой 
      nodeSelector:
        <KUBE_LABEL_DEPLOYMENT>
      restartPolicy: Always

---
apiVersion: v1
# Разворачиваем сервис, который будет пробрасывать порты контейнера в ноду
kind: Service
metadata:
  labels:
    # Метка сервиса, через нее будут идти обращения в поды Django из других контейнеров
    app: <KUBE_SERVICE_NAME>
  # Представление сервиса
  name: <KUBE_SERVICE_NAME>
  # Пространство имен тоже что и у проекта
  namespace: <K8S_NAMESPACE>
spec:
  selector:
    # Метка пода из matchLabels проекта
    app: <KUBE_NAME>
  ports:
    - protocol: TCP
    # Название порта
      name: web-port
      port: 80
      targetPort: 80
' >kubernetes/web-deployment.yaml

		sed -i -e "s,<KUBE_NAME>,$KUBE_NAME,g" kubernetes/web-deployment.yaml
		sed -i -e "s,<KUBE_SERVICE_NAME>,$KUBE_SERVICE_NAME,g" kubernetes/web-deployment.yaml
		sed -i -e "s,<KUBE_IMAGE_PULL>,$KUBE_IMAGE_PULL,g" kubernetes/web-deployment.yaml
		sed -i -e "s,<KUBE_LABEL_DEPLOYMENT>,$KUBE_LABEL_DEPLOYMENT,g" kubernetes/web-deployment.yaml

	fi

}

createRedisDeployment() {
	#Create redis deployment

	if [[ -d kubernetes/redis-deployment.yaml ]]; then
		echo "${red}[-] redis_deployment.yam already exist${reset}"
	else
		echo "${green}[+] Create redis_deployment.yam${reset}"
		touch kubernetes/redis-deployment.yaml
		echo '
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis
  namespace: <KUBE_NS>
  labels:
    deployment: redis
spec:
  selector:
    matchLabels:
      pod: redis
  replicas: 1
  template:
    metadata:
      labels:
        pod: redis
    spec:
      containers:
        - name: redis
          image: redis
          resources:
            limits:
              cpu: 20m
              memory: 200Mi
            requests:
              cpu: 5m
              memory: 10Mi
          ports:
          - containerPort: 6379
      nodeSelector:
        <KUBE_LABEL_DEPLOYMENT> 

---
apiVersion: v1
kind: Service
metadata:
  name: redis-service
  namespace: <KUBE_NS>
spec:
  selector:
    pod: redis
  ports:
  - protocol: TCP
    port: 6379
    targetPort: 6379
' >kubernetes/redis-deployment.yaml

		sed -i -e "s,<KUBE_NS>,$KUBE_NS,g" kubernetes/redis-deployment.yaml
		sed -i -e "s,<KUBE_LABEL_DEPLOYMENT>,$KUBE_LABEL_DEPLOYMENT,g" kubernetes/redis-deployment.yaml
	fi
}

createWebConfigMap() {
	#Create configmap for web
	if [[ -d kubernetes/config-map ]]; then
		echo "${green}[+] All config will be created in the kubernetes/config-map folder${reset}"
	else
		mkdir -p kubernetes/config-map
	fi

	if [[ -d kubernetes/config-map/web-configmap.yaml ]]; then
		echo "${red}[-] kubernetes/config-map/web-configmap.yaml already exist${reset}"
	else
		echo "${green}[+] Create web-configmap.yaml${reset}"
		echo '
apiVersion: v1
kind: ConfigMap
metadata:
  # Название configmap которое указавали в диплоймент выше
  name: env
  namespace: <KUBE_NS>
data:
# Переменнве указываем в формате yaml
# Каждая переменная с новой строки
  DOMAIN_NAME: <DEV_DOMAIN>
  POSTGRES_HOST: "postgres-service.main.svc.cluster.local"
  POSTGRES_PORT: "5432"' >kubernetes/config-map/web-configmap.yaml

		sed -i -e "s,<DEV_DOMAIN>,$DEV_DOMAIN,g" kubernetes/config-map/web-configmap.yaml
		sed -i -e "s,<KUBE_NS>,$KUBE_NS,g" kubernetes/config-map/web-configmap.yaml
	fi
}

createNginxConfigMap() {
	#Create configmap for nginx

	if [[ -d kubernetes/nginx-config ]]; then
		echo "${green}[+] All nginx config files will be created in the kubernetes/nginx-config folder${reset}"
	else
		mkdir -p kubernetes/nginx-config
	fi

	if [[ -d kubernetes/nginx-config/nginx-configmap.yaml ]]; then
		echo "${red}[-] kubernetes/nginx-config/nginx-configmap.yaml already exist${reset}"
	else
		echo "${green}[+] Create nginx-configmap.yaml${reset}"
		echo '
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-config
  namespace: <KUBE_NS>
data:
  default.conf: |
    upstream web {
        server localhost:8000;
    }

    server {
        listen 80;
        listen [::]:80;

        client_max_body_size 100M;
        
        server_name <DEV_DOMAIN>;

        location / {
            proxy_pass http://web ;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $host;
            proxy_redirect off;
        }

        location /static/ {
            alias /app/static/;
        }
    }
        ' >kubernetes/nginx-config/nginx-configmap.yaml

		sed -i -e "s,<DEV_DOMAIN>,$DEV_DOMAIN,g" kubernetes/nginx-config/nginx-configmap.yaml
		sed -i -e "s,<KUBE_NS>,$KUBE_NS,g" kubernetes/nginx-config/nginx-configmap.yaml
	fi
}

createClusterIssure() {
	#Create Cluster Issure

	if [[ -d kubernetes/cluster-issure.yaml ]]; then
		echo "${red}[-] kubernetes/cluster-issure.yaml already exist${reset}"
	else
		echo "${green}[+] Create cluster-issure.yaml${reset}"
		echo '
apiVersion: cert-manager.io/v1
# Содание эмитента для получения сертификата Letsencrypt
kind: ClusterIssuer
metadata:
  # Название эмитента обязательно использовать это название letsencrypt-prod
  name: letsencrypt-prod
  namespace: <KUBE_NS>
spec:
  # Automated Certificate Management Environment (ACME)
  # При определении ACME cert-manager будет автоматически генерировать privat key
  # для идентификации пользователя на ACME сервере
  acme:
    # Ссылка для получения сертификата
    server: https://acme-v02.api.letsencrypt.org/directory
    email: <KUBE_USER_EMAIL>
    # Подключение приватного ключа для эмитента
    privateKeySecretRef:
      # Название эмитента
      name: prod-issuer-account-key
    # Добавьте единственное средство решения проблем, HTTP01, используя nginx
    solvers:
    - http01:
        ingress:
          class: nginx
    ' >kubernetes/cluster-issure.yaml

		sed -i -e "s,<KUBE_USER_EMAIL>,$KUBE_USER_EMAIL,g" kubernetes/cluster-issure.yaml
		sed -i -e "s,<KUBE_NS>,$KUBE_NS,g" kubernetes/cluster-issure.yaml
	fi
}

createIngress() {
	#Create ingress
	if [[ -d kubernetes/ingress-rules.yaml ]]; then
		echo "${red}[-] kubernetes/ingress-rules.yaml already exist${reset}"
	else
		echo "${green}[+] Create ingress-rules.yaml${reset}"
		echo '
apiVersion: networking.k8s.io/v1
#Правила маршрутизации ingress
kind: Ingress
metadata:
  # Название ingresss
  name: ingress-rule
  # Пространство имен где будет развернут ingress
  namespace: <KUBE_NS>
  annotations:
    nginx.ingress.kubernetes.io/proxy-body-size: 30m
    # Анатации необходимые для подключени cert-manager и ingress-nginx
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: / 
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
    ingress.kubernetes.io/ssl-redirect: "true"

spec:
  tls:
  # Указываем cert-manager какому хосту выписать сертифика 
    - hosts:
      - <DEV_DOMAIN>
      # Название которое будет присвоино сертификату
      secretName: <KUBE_SECRET>
  rules:
  # Перенаправляем трафик в наше приложение
  - host: <DEV_DOMAIN>
    http:
      paths:
        - pathType: Prefix
          path: "/"
          backend:
            # Сервис нашего прилочения
            service:
              name: <KUBE_SERVICE_NAME>
              port: 
                number: 80
          ' >kubernetes/ingress-rules.yaml

		sed -i -e "s,<KUBE_SERVICE_NAME>,$KUBE_SERVICE_NAME,g" kubernetes/ingress-rules.yaml
		sed -i -e "s,<KUBE_SECRET>,$KUBE_SECRET,g" kubernetes/ingress-rules.yaml
		sed -i -e "s,<KUBE_NS>,$KUBE_NS,g" kubernetes/ingress-rules.yaml
		sed -i -e "s,<DEV_DOMAIN>,$DEV_DOMAIN,g" kubernetes/ingress-rules.yaml
	fi
}

createGitLabCI() {

	if [[ -d .gitlab-ci.yml ]]; then
		echo "${red}[-] .gitlab-ci.yml already exist${reset}"
	else
		echo "${green}[+] Create .gitlab-ci.yml${reset}"
		echo '
# Приложение собирается внутри gitlab образ тоже храниться в gitlab
.common_script_build: &common_script_build
  services:
    - docker:19.03.12-dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
    # Эту переменную указываем если во время сборки нужно подтянуть проект из другого репозитория
    GIT_SUBMODULE_STRATEGY: recursive
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    docker build -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG.$CI_PIPELINE_ID" .;
    docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG.$CI_PIPELINE_ID";

.common_script_deploy: &common_script_deploy
  image: 
    name: bitnami/kubectl:1.20
    entrypoint: [""]
  before_script:
  # Set kubernetes credentials
    - export KUBECONFIG=/tmp/kubeconfig
    - kubectl config set-cluster $KUBERNETES_CLUSTER --insecure-skip-tls-verify=true --server="$(echo $KUB_SERVER)"
    - kubectl config set-credentials $KUBERNETES_CLUSTER_USER --token="$(echo $K8S_CI_TOKEN | base64 -d)"
    - kubectl config set-context $KUBERNETES_CLUSTER_USER --cluster=$KUBERNETES_CLUSTER --user=$KUBERNETES_CLUSTER_USER  
    - kubectl config use-context $KUBERNETES_CLUSTER_USER
  script:
    sed -i -e "s,<IMAGE>,$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG.$CI_PIPELINE_ID,g" kubernetes/web-deployment.yaml;
    sed -i -e "s,<K8S_NAMESPACE>,$K8S_NAMESPACE,g" kubernetes/web-deployment.yaml;
    sed -i -e "s,<K8S_ENV>,$K8S_ENV,g" kubernetes/web-deployment.yaml;
    kubectl apply -f kubernetes/web-deployment.yaml;


build_kube:
  # образ докера
  image: docker:19.03.12
  # Указываем что будем билдить
  stage: build
  # Переменое окружение из корого будут браться переменные обычно соответствует названию ветки
  environment:
    name: dev
  <<: *common_script_build
  only:
    - dev
    
deploy_kube:
  stage: deploy
  environment:
    name: dev
  <<: *common_script_deploy
  only:
    - dev
  ' >.gitlab-ci.yml
	fi
}

setGitLabVariables() {
	echo "${green}[+] Variables to be created in ${reset}${red}GitLab > CI/CD > Variables${reset}"
	echo -e "${green}\nKUBERNETES_CLUSTER\nKUBERNETES_CLUSTER_USER\nKUB_SERVER\nK8S_ENV\n${reset}"
	echo "${green}K8S_NAMESPACE: $KUBE_NS ${reset}"
	echo -e "${green}K8S_CI_TOKEN: ${reset}\n"
	echo $TOKEN
}

getShellName
setEnvironmentVariables

createImagePullPolicy
createImagePullSecret

#Create Deployment files
createWebDeployment
createRedisDeployment
createWebConfigMap
createNginxConfigMap
createClusterIssure
createIngress
createGitLabCI
setGitLabVariables
